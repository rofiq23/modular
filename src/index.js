import Table from "./Table"

const table = new Table({
  columns: ["Nama", "Category","Stock"],
  data: [
    {item:"Mangga Manalagi", cat:"Buah",stock:30},
    {item:"Pakcoy", cat:"Sayur",stock:10}
    // ["Pakcoy", "Sayur","40"]
  ]
});

const app = document.getElementById("app");
table.render(app);
var showForm = false;
var buttonAdd = document.getElementById('tambahData');
var tampil = document.getElementById('tampil');
var tex = document.getElementById('tes1')

tampil.addEventListener('click',function(){
    showForm = !showForm;
  tex.style.display= showForm == true ?'block':'none';
  tampil.textContent = showForm == true ?'Close Formulir':'Input by Form';
})
document.getElementById("formInput").addEventListener("submit", function(event) {
  event.preventDefault();

  var itemInput = document.getElementById("item").value;
  var catInput = document.getElementById("category").value;
  var stockInput = document.getElementById("stock").value;
  let [...tempArr] = table.init.data;

  // Menambahkan objek baru ke dalam array
  var newObj = {item:itemInput,cat:catInput,stock:stockInput};
  table.init.data = [...tempArr, newObj];
  table.render(app);

  // Mengosongkan input setelah data ditambahkan
  document.getElementById("item").value = '';
  document.getElementById("stock").value = '';
});

buttonAdd.addEventListener('click',function(){
//   let tempData={};
  let [...tempArr] = table.init.data;
  let nama = prompt("Masukan Nama Item :");
  if(nama !== null){
    let category = prompt("Masukan Category Item :");
    let stock = parseInt(prompt("Masukan Stock Item :"));
    let input = confirm("yakin ingin memasukan data ?")
    if(input){
        let tempData = {item:nama,cat:category,stock:stock};
        // tempData.push(category);
        // tempData.push(stock);
        table.init.data = [...tempArr, tempData];
        table.render(app);
    }
  }else{
    alert("anda tidak jadi input data.\n terimakasih")
  }
  // console.log(tempData)
  console.log(table.init.data)
  tempData=[]
});


// let test = ["nama","name@mail.com"]
// table.init.data.push(test)
// console.log(table.init.data)
// table.render(app);